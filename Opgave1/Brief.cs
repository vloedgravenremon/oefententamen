﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OefenTentamenenCsharp
{
    public class Brief : Post
    {
        public bool Aangetekend { get; set; }

        public Brief(string naam, string postcode, int huisnummer, bool aangetekend) : base(naam, postcode, huisnummer)
        {
            Aangetekend = aangetekend;
        }

        public override string Label()
        {
            if (Aangetekend)
            {
                return $"Aangetekende brief voor {base.Label()}";

            }
            return $"Brief voor {base.Label()}";
        }
    }
}
