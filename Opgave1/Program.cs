﻿using OefenTentamenenCsharp;
using System;

namespace Opgave1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Opgave 1b
            
            
            Pakketje pakketje = new Pakketje("Freek", "5466UP", 23);
            Pakketje pakketje2 = new Pakketje("Martijn", "1234ZP", 87, 12, Pakketje.Doos.Groot);
            Brief brief = new Brief("Henk", "7890QW", 25, false);
            Brief brief2 = new Brief("Windesheim", "8001AA", 25, true);

            Console.WriteLine(pakketje.Label());
            Console.WriteLine(pakketje2.Label());
            Console.WriteLine(brief.Label());
            Console.WriteLine(brief2.Label());
            
            // Opgave 1g

            
            PostKantoor postKantoor = new PostKantoor("Zwolle");

            Bezorger ernst = new Bezorger("Ernst", postKantoor);
            postKantoor.Aannemen(pakketje);
            postKantoor.Aannemen(brief);
            
        }
    }
}
