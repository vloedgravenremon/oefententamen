﻿using System;
using System.Collections.Generic;
using System.Text;
using OefenTentamenenCsharp;

namespace Opgave1
{
    public class PostEventArgs : EventArgs
    {
        public Post Post { get; set; }
    }
}
