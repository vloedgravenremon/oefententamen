﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OefenTentamenenCsharp
{
    public class Pakketje : Post
    {
        public enum Doos
        {
            Brievenbus,
            Klein,
            Groot
        }

        private double _gewicht;
        public double Gewicht
        {
            get
            {
                return _gewicht;
            }
            set
            {
                if (value < 0 || value > 15)
                {
                    throw new ArgumentOutOfRangeException();
                }

                _gewicht = value;
            }
        }
        public Doos Afmeting { get; set; }

        public Pakketje(string naam, string postcode, int huisnummer) : base(naam, postcode, huisnummer)
        {
            Gewicht = 2;
            Afmeting = Doos.Brievenbus;
        }

        public Pakketje(string naam, string postcode, int huisnummer, double gewicht, Doos afmeting) : base(naam, postcode, huisnummer)
        {
            Gewicht = gewicht;
            Afmeting = afmeting;
        }

        public override string Label()
        {
            return $"{Afmeting} pakketje van {Gewicht} kilo voor {base.Label()}";
        }
    }
}
