﻿using OefenTentamenenCsharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Opgave1
{
    public class Bezorger
    {
        public string Naam { get; set; }

        public Bezorger(string naam, PostKantoor postKantoor)
        {
            Naam = naam;
            postKantoor.NieuwePost += PostKantoor_NieuwePost;
        }

        public void PostKantoor_NieuwePost(object sender, EventArgs e)
        {
            PostEventArgs args = (PostEventArgs) e;
            PostKantoor kantoor = (PostKantoor) sender;
            Console.WriteLine($"Bezorger {Naam}: bezorgen {args.Post.Label()} ophalen bij postkantoor {kantoor.Plaats}");
        }
    }
}
