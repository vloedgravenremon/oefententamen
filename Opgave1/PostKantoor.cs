﻿using System;
using System.Collections.Generic;
using System.Text;
using OefenTentamenenCsharp;

namespace Opgave1
{
    public class PostKantoor
    {
        public event EventHandler NieuwePost;
        public string Plaats { get; set; }

        public PostKantoor(string plaats)
        {
            Plaats = plaats;
        }

        public void Aannemen(Post post)
        {
            NieuwePost?.Invoke(this, new PostEventArgs(){Post = post});
        }
    }
}
