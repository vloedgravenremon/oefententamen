﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OefenTentamenenCsharp
{
    public abstract class Post
    {
        public string Naam { get; set; }
        public string Postcode { get; set; }
        public int Huisnummer { get; set; }

        public Post(string naam, string postcode, int huisnummer)
        {
            Naam = naam;
            Postcode = postcode;
            Huisnummer = huisnummer;
        }

        public virtual string Label()
        {
            return Naam + " " + Postcode + " " + Huisnummer;
        }
    }
}
