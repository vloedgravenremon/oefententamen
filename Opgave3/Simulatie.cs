﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Opgave3
{
    public class Simulatie
    {
        private bool[,] _bord;

        public Simulatie(bool[,] start)
        {
            _bord = start;
        }

        public string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < _bord.GetLength(0); i++)
            {
                for (int j = 0; j < _bord.GetLength(1); j++)
                {
                    stringBuilder.AppendLine(_bord[i, j] ? "0" : ".");
                }
                stringBuilder.AppendLine("");
            }
            return stringBuilder.ToString();
        }

        //LETOP DE MIN 1
        public bool CellStatus(int x, int y)
        {
            if (x < _bord.GetLength(0)-1 && x > 0 && y < _bord.GetLength(1)-1 && y > 0)
            {
                return _bord[x, y];
            }
            return false;
        }

        // ipv for loops 8 if statements
        public int AantalBuren(int x, int y)
        {
            int aantal = 0;
            if (CellStatus(x - 1, y - 1)) aantal++;
            if (CellStatus(x, y - 1)) aantal++;
            if (CellStatus(x + 1, y - 1)) aantal++;
            if (CellStatus(x - 1, y)) aantal++;
            if (CellStatus(x + 1, y)) aantal++;
            if (CellStatus(x - 1, y + 1)) aantal++;
            if (CellStatus(x, y + 1)) aantal++;
            if (CellStatus(x + 1, y + 1)) aantal++;

            return aantal;
        }

        public bool Nieuw(int x, int y)
        {
            return AantalBuren(x, y) == 3;
        }
        public bool Blijft(int x, int y)
        {
            return AantalBuren(x, y) == 3 || AantalBuren(x,y) == 2;
        }

        public void VolgendeStap()
        {
            bool[,] nieuwBord = new bool[_bord.GetLength(0), _bord.GetLength(1)];
            for (int i = 0; i < _bord.GetLength(0); i++)
            {
                for (int j = 0; j < _bord.GetLength(1); j++)
                {
                    nieuwBord[i, j] = _bord[i, j] ? Blijft(i, j) : Nieuw(i, j);
                }
            }

            _bord = nieuwBord;
        }
    }
}
