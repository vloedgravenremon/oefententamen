﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace Opgave2
{
    public class Map<T> where T : IBestand
    {
        public Func<T, string, bool> NaamFilter = (bestand, s1) =>
        {
            return bestand.Naam.Equals(s1);
        };
        //Casten naar IDocument want auteur staat in IDocument
        public Func<T, string, bool> AuteurFilter = (bestand, auteur) =>
        {
            return bestand is IDocument ? ((IDocument) bestand).Auteur.ToLower().Equals(auteur.ToLower()) : false;
        };
        private List<T> _bestanden;

        public Map()
        {
            _bestanden = new List<T>();
        }
        public void Toevoegen(T bestand)
        {
            _bestanden.Add(bestand);
        }

        public void Verwijderen(T bestand)
        {
            _bestanden.Remove(bestand);
        }

        //LINQ
        public int TotaleOmvang()
        {
            return _bestanden.Sum(b => b.Omvang);
        }

        //LINQ query:
        public int TotaleOmvang(T type)
        {
            return _bestanden.FindAll(b => b is T).Sum(b => b.Omvang);
        }

        //Correcte LINQ method syntax
        public List<T> Selecteren(string waarde, Func<T, string , bool> filter)
        {
            return (List<T>)(from bestand in _bestanden
                where filter(bestand, waarde)
                orderby bestand.Naam descending
                select bestand);
        }
    }
}
