﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Opgave2
{
    public interface IBestand
    {
        public string Naam { get; set; }
        public int Omvang { get; set; }

        public bool Equals(object t);
    }
}
