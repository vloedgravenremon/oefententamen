﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Opgave2
{
    public class Word : IDocument, IBestand
    {
        public string Auteur { get; set; }
        public string Naam { get; set; }
        public int Omvang { get; set; }
        public bool Equals(object t)
        {
            return t.GetType() == this.GetType();
        }
    }
}
