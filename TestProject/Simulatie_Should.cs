using System.Text;
using NUnit.Framework;
using Opgave3;

namespace TestProject
{
    public class Simulatie_Should
    {
        bool[,] bord = new bool[,]{
            {false,false,false,false},
            {false,true,true,false},
            {false,true,true,false},
            {false,false,false,false}
        };

        private Simulatie simulatie;
        [SetUp]
        public void Setup()
        {
            simulatie = new Simulatie(bord);
        }

        [Test]
        public void CellStatus_Should_Return()
        {
            bool test1 = simulatie.CellStatus(1, 1);
            bool test2 = simulatie.CellStatus(4, 4);

            Assert.IsTrue(test1);
            Assert.IsFalse(test2);
        }

        [Test]
        public void AantalBuren_Should_Return()
        {
            int test1 = simulatie.AantalBuren(0, 0);
            int test2 = simulatie.AantalBuren(1, 1);

            int antwoord1 = 1;
            int antwoord2 = 3;

            Assert.AreEqual(antwoord1, test1);
            Assert.AreEqual(antwoord2, test2);
        }

        [Test]
        public void ToString_Should_Return()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append(".");
            stringBuilder.Append(".");
            stringBuilder.Append(".");
            stringBuilder.Append(".");
            stringBuilder.AppendLine("");
            stringBuilder.Append(".");
            stringBuilder.Append("O");
            stringBuilder.Append("O");
            stringBuilder.Append(".");
            stringBuilder.AppendLine("");
            stringBuilder.Append(".");
            stringBuilder.Append("O");
            stringBuilder.Append("O");
            stringBuilder.Append(".");
            stringBuilder.AppendLine("");
            stringBuilder.Append(".");
            stringBuilder.Append(".");
            stringBuilder.Append(".");
            stringBuilder.Append(".");
            stringBuilder.AppendLine("");

            string antwoord = stringBuilder.ToString();

            string test = simulatie.ToString();

            Assert.AreEqual(antwoord, test);
        }
        [Test]
        public void VolgendeStap()
        {

        }
    }
}